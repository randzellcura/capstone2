const mongoose = require('mongoose')

const order_schema = new mongoose.Schema({
    products: [
        {
            userId: {
                type: String,
                required: [true, 'User ID is required.']
            },
            productId: {
                type: String,
                required: [true, 'Product ID is required.']
            },
            quantity: {
                type: Number,
                required: false
            }
        }
    ],
    totalAmount: {
        type: Number,
        required: false
    },
    purchedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Orders', order_schema)