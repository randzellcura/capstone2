const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const cors = require('cors')

const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')

dotenv.config()

const app = express()
const port = process.env.PORT || 8005

// Connect to MongoDB
mongoose.connect(`mongodb+srv://ivanc:${process.env.MONGODB_PASSWORD}@cluster0.asallxv.mongodb.net/e-commerce-api?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection
db.once('open', () => console.log('Connected to MongoDB'))

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/orders', orderRoutes)

app.listen(port, () => {
    console.log(`E-commerce API is now running on localhost: ${port}`)
})