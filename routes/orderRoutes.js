const express = require('express')
const router = express.Router()
const OrderController = require('../controllers/OrderController')
const auth = require('../auth')

// Create order
router.post('/create', auth.verify, (request, response) => {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        order: request.body,
        userId: auth.decode(request.headers.authorization).userId
    }
    OrderController.addOrder(data).then((result) => {
        response.send(result)
    })
})

// Retrieve all orders
router.get('/', auth.verify, (request, response) => {
    const data = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    OrderController.getAllOrders(data).then((result) => {
        response.send(result)
    })
})

// Remove order

// Update order

module.exports = router