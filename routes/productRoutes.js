const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')

// Admin functions

// Add product (Admin only)
router.post('/addProduct', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	ProductController.addProduct(data).then((result) => {
		response.send(result)
	})
})

// Update product (Admin only)
router.patch('/update/:id', auth.verify, (request, response) => {
	const data = {
		id: request.params.id,
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	ProductController.updateProduct(data).then((result)=> {
		response.send(result)
	})
})

// Archive product (Admin only)
router.patch('/archive/:id', auth.verify, (request,response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin
	ProductController.archiveProduct(request.params.id, isAdmin).then((result) => {
		response.send(result)
	})
})


// Normal User functions

// Get all products
router.get('/', (request, response) => {
	ProductController.getAllProducts().then((result) => {
		response.send(result)
	})
})

// Get all active products
router.get('/active', (request, response) => {
	ProductController.getAllActive().then((result) => {
		response.send(result)
	})
})

// Get a specific product by ID
router.get('/:productId', (request, response) => {
	ProductController.getProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})

module.exports = router