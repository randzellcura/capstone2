const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')

// Check if email exists
router.post("/check-email", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	UserController.checkIfEmailExists(request.body, data).then((result) => {
		response.send(result)
	})
})

// Register new user
router.post("/register", auth.verify, (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

// Get user details
router.get("/:id/details", auth.verify, (request, response) => {
	UserController.getUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})

// Login user
router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

// Update a user
router.patch('/update/:userId', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.updateUser(request.params.userId, data).then((result) => {
		response.send(result)
	})
})

// Get all users 
router.get('/', auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	UserController.getAllUsers(data).then((result) => {
		response.send(result)
	})
})


module.exports = router